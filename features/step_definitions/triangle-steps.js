'use strict';

const facade = require('../support/facade');
const expect = require('chai').expect;

var {defineSupportCode} = require('cucumber');

defineSupportCode(({Given, When, Then}) => {


    let triangleType = undefined;

    const calculateTriangleType = async (sides) => {
        // Do a correct request.
        triangleType = await facade.doTriangleCalculation(sides);
    };


    // Step definition for precondition
    Given(/^Input calculator is a available$/, function () {
        return facade.prepareEnvironment();
    });

    // --------------------------------------------------------------- /

    // Step definitions for request
    When(/^(\d+) equals sides are inputted$/, calculateTriangleType);

    // --------------------------------------------------------------- //

    // Step definitions for expected results
    Then(/^the triangle is qualified as '(\w+)'$/, function (expectedTriangleType) {
        expect(triangleType).to.equal(expectedTriangleType);
        return;
    });
});